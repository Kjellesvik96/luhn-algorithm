package main;

import main.algorithms.LuhnAlgorithm;

import java.util.NoSuchElementException;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
		LuhnAlgorithm luhn = new LuhnAlgorithm();

		String ccNumber = requestNumber();
		String ccDigits = ccNumber.substring(0, ccNumber.length() - 1);
		String providedLastDigit = ccDigits.substring(ccDigits.length() - 1);
		String expectedLastDigit = luhn.calculate(ccDigits);

		displayOutput(ccNumber, providedLastDigit, expectedLastDigit);
	}

	// The first method you interfere with, lets you enter your number
	public static String requestNumber() {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter your credit card number:");
		String creditCard = "";
		try {
			creditCard = scanner.next();
		} catch (NoSuchElementException e) {
			System.out.println("ERROR!");
		}
		return creditCard;
	}

	// Method to display "results"
	private static void displayOutput(String ccNumber, String providedLastDigit, String expectedLastDigit) {
		LuhnAlgorithm luhn = new LuhnAlgorithm();
    	System.out.println("Input: " + ccNumber);
		System.out.println("Provided: " + providedLastDigit);
		System.out.println("Expected: " + expectedLastDigit);
		System.out.println("Checksum: " + luhn.checksum(expectedLastDigit, providedLastDigit));

		if (luhn.checkCreditCard(ccNumber)) {
			System.out.println("Number of digits: " + ccNumber.length() + " (credit card)");
		} else {
			System.out.println("Number of digits: " + ccNumber.length() + " (not a credit card)");
		}
    }
}