package main.algorithms;

public class LuhnAlgorithm {

    public LuhnAlgorithm() {}

    /* Function that loops through list of digits from right to left.
     * On every other element it doubles the number.
     * If the number is above 9 it gets handled.
     * Returns the "reminder" which should be equal to the last number you provided
     */
    public String calculate(String ccDigits) {
        char[] digitList = ccDigits.toCharArray();
        int total = 0;
        boolean skipEveryOther = false;
        for (int i = (ccDigits.length()-1); i >= 0; i--) {
            int digit = Integer.parseInt(String.valueOf(digitList[i]));
            if (!skipEveryOther){
                digit = doubleDigit(digit);
                if (tenOrAbove(digit)) {
                    digit -= 9;
                }
                skipEveryOther = true;
            } else  {
                skipEveryOther = false;
            }
            total += digit;
        }
        return Integer.toString((total * 9) % 10);
    }

    // Function that doubles the numbers
    public int doubleDigit(int digit){
        return digit * 2;
    }

    // Checker for numbers that needs adjustment
    public Boolean tenOrAbove(int digit){
        return digit > 9;
    }

    //Function that checks the last digit is correct
    public String checksum(String expectedLastDigit, String providedLastDigit) {
        if (!expectedLastDigit.equals(providedLastDigit)) {
            return "Not valid";
        }
        return "Valid";
    }

    //Function to validate if the amount of digits is correct
    public boolean checkCreditCard(String ccNumber) {
        int numberOfDigits = ccNumber.length();
        return numberOfDigits == 16;
    }
}