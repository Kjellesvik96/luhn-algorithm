package test;

import main.algorithms.LuhnAlgorithm;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class LuhnTest {

    LuhnAlgorithm luhn = new LuhnAlgorithm();

    @Test
    void checksumEqualNumbers() {
        assertEquals("Valid", luhn.checksum("4", "4"));
        assertEquals("Valid", luhn.checksum("6", "6"));
    }

    @Test
    void checksumNotEqualNumbers() {
        assertEquals("Not valid", luhn.checksum("3", "6"));
        assertEquals("Not valid", luhn.checksum("2", "4"));
    }

    @Test
    void calculateLuhn() {
        assertEquals("2", luhn.calculate("424242424242424"));
        assertEquals("7", luhn.calculate("134943744343448"));
        assertNotEquals("3", luhn.calculate("6546765443545"));
        assertNotEquals("1", luhn.calculate("18288394565944"));
    }

    @Test
    void CheckCreditCardLength() {
        assertTrue(luhn.checkCreditCard("4565458325965845"));
        assertTrue(luhn.checkCreditCard("4738278577685457"));
        assertFalse(luhn.checkCreditCard("4166358596880"));
        assertFalse(luhn.checkCreditCard("47535395936845890646"));
    }

    @Test
    void checkIfTooHigh() {
        assertTrue(luhn.tenOrAbove(10));
        assertTrue(luhn.tenOrAbove(12));
        assertFalse(luhn.tenOrAbove(0));
        assertFalse(luhn.tenOrAbove(9));
    }

    @Test
    void doubleDigits() {
        assertEquals( 18, luhn.doubleDigit(9));
        assertEquals( 14, luhn.doubleDigit(7));
        assertEquals( 8, luhn.doubleDigit(4));
        assertEquals( 4, luhn.doubleDigit(2));
    }
}
